import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main implements NativeKeyListener {
    //flags to make sure both keys are pressed
    private boolean key1 = false;
    private boolean key2 = false;
    //robot to make the computer do mouse commands
    Robot bot;
    {
        try {
            bot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    //does nothing has to be here for some reason...maybe i'll look into it later??
    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
        System.out.println("test");
    }

    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
//checks to see if the keys are pressed
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_ALT) {
            //flips the flags to true
            key1 = true;
        } else if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_1) {
            key2 = true;
        }
//if both flags are true hasthe bot run commands to save images
        if (key1 == true && key2 == true) {
//Button 3 is rightclick need to do both press AND release
            bot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
            bot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
//need a delay or runs the commands to fast
            //issue with this delay not being as short as i wanted
            bot.delay(100);
//V is the hotkey for browsers to save again press and release
            bot.keyPress(KeyEvent.VK_V);
            bot.keyRelease(KeyEvent.VK_V);
//for some reason this delay seems to effect the first delay??
            bot.delay(800);
//enter just to save as fast boi
            bot.keyPress(KeyEvent.VK_ENTER);
            bot.keyRelease(KeyEvent.VK_ENTER);
        }

        //This ends the script, god key
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
            try {
                GlobalScreen.unregisterNativeHook();
            } catch (NativeHookException e) {
                e.printStackTrace();
            }
        }
    }

    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
//Needs to check keys are released to flip the flag back otherwise u get an infinate loop
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_ALT) {
            key1 = false;
        } else if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_1) {
            key2 = false;
        }
    }
    public static void main(String[] args) throws AWTException {
        Logger log = Logger.getLogger(GlobalScreen.class.getPackage().getName());
//prevents the script from showing stuff such as mouse movements
        log.setLevel(Level.OFF);
//idk what this is, was here when i got here
//        log.setUseParentHandle rs(false);

        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
            System.exit(1);
        }

        GlobalScreen.addNativeKeyListener(new Main());
    }
}
    
    
    
